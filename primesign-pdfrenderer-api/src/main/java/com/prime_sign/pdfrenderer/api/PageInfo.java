package com.prime_sign.pdfrenderer.api;

/**
 * Reflects basic information about a specific document page.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
public class PageInfo {

	private float width;
	private float height;

	/**
	 * Returns the page's width (in pdf units).
	 * 
	 * @return The width in pdf units.
	 */
	public float getWidth() {
		return this.width;
	}

	/**
	 * Sets the page's width (in pdf units).
	 * 
	 * @param width The page's width (in pdf units).
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**
	 * Returns the page's height (in pdf units).
	 * 
	 * @return The height in pdf units.
	 */
	public float getHeight() {
		return this.height;
	}

	/**
	 * Sets the page's height (in pdf units).
	 * 
	 * @param height The page's height (in pdf units).
	 */
	public void setHeight(float height) {
		this.height = height;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(height);
		result = prime * result + Float.floatToIntBits(width);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PageInfo other = (PageInfo) obj;
		if (Float.floatToIntBits(height) != Float.floatToIntBits(other.height))
			return false;
		if (Float.floatToIntBits(width) != Float.floatToIntBits(other.width))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PageInfo [width=");
		builder.append(this.width);
		builder.append(", height=");
		builder.append(this.height);
		builder.append("]");
		return builder.toString();
	}

}
