package com.prime_sign.pdfrenderer.api;

/**
 * Specific exception for the pdf renderer.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 */
public class PdfRendererException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Error code indicating that a page (number) has been requested to be rendered that does not exist.
	 */
	public static final int ERROR_INVALID_PAGE_NO = 101;

	/**
	 * Error code indicating that the pdf renderer was unable to render a certain page.
	 */
	public static final int ERROR_UNABLE_TO_RENDER_PAGE = 102;

	/**
	 * The error code associated with this specific exception.
	 */
	private int errorCode;

	/**
	 * Creates an exception with {@code errorCode} and error {@code message}.
	 * 
	 * @param errorCode The error code.
	 * @param message   The error message.
	 */
	public PdfRendererException(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Creates an exception with {@code errorCode} and error {@code cause}.
	 * 
	 * @param errorCode The error code.
	 * @param cause     The cause.
	 */
	public PdfRendererException(int errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}

	/**
	 * Creates an exception with {@code errorCode}, {@code message} and {@code cause}.
	 * 
	 * @param errorCode The error code.
	 * @param message   The error message.
	 * @param cause     The cause.
	 */
	public PdfRendererException(int errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	/**
	 * Returns the error code for this exception.
	 * 
	 * @return The error code.
	 */
	public int getErrorCode() {
		return this.errorCode;
	}

	@Override
	public String toString() {
		return super.toString() + " (Error code: " + this.errorCode + ")";
	}

}
