package com.prime_sign.pdfrenderer.api;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.WillNotClose;

/**
 * Factory interface for creation of certain pdf renderer factory implementations.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ParametersAreNonnullByDefault
public interface PdfRendererFactory {

	/**
	 * Creates a pdf renderer using a given input stream (representing the raw pdf document).
	 * 
	 * @param pdfDocument Inputstream representing the pdf document (required; must not be {@code null}).
	 * @return A pdf renderer instance (never {@code null}).
	 * @throws IOException          Thrown in case of I/O error reading the provided input stream.
	 * @throws PdfRendererException Unchecked exception that may be thrown in case of error parsing the provided document.
	 */
	@Nonnull PdfRenderer createRenderer(@WillNotClose InputStream pdfDocument) throws IOException;

}
