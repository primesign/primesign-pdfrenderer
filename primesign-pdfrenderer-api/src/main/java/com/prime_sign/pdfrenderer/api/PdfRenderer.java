package com.prime_sign.pdfrenderer.api;

import java.awt.image.BufferedImage;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;

/**
 * Interface for a pdf rendering module.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 */
public interface PdfRenderer extends AutoCloseable {

	/**
	 * Renders a certain page of the pdf document with a certain zoom factor and returns an image object.
	 * 
	 * @param page The number of the page to be rendered. Any number &lt; 1 or &gt; {@link #getPageCount()} SHOULD throw a
	 *             {@link PdfRendererException} with error code {@link PdfRendererException#ERROR_INVALID_PAGE_NO}.
	 * @param zoom The zoom factor of the rendered image in relation to the original pdf page.
	 * @return The rendered image (never {@code null}). Hint: Use
	 *         {@link ImageIO#write(java.awt.image.RenderedImage, String, java.io.OutputStream)} to get the specific image
	 *         instance ({@code png}, {@code jpg}, {@code tif}...).
	 * @throws PdfRendererException Unchecked exception thrown in case of error. Refer to {@link PdfRendererException} for specific error codes.
	 */
	@Nonnull
	BufferedImage renderPage(int page, float zoom);

	/**
	 * Returns the number of pages of the specific pdf document.
	 * 
	 * @return An integer representing the number of pages.
	 * @throws PdfRendererException Unchecked exception thrown in case of error. Refer to {@link PdfRendererException} for specific error codes.
	 */
	int getPageCount();

	/**
	 * Returns info on a certain page (width, height...). This method is thought to be OPTIONAL. If not supported an
	 * {@link UnsupportedOperationException} should be thrown.
	 * 
	 * @param page The number of the page (1..n).
	 * @return Information on page {@code page}. (never {@code null})
	 * @throws PdfRendererException Unchecked exception thrown in case of error. Refer to {@link PdfRendererException} for specific error codes.
	 */
	@Nonnull
	PageInfo getPageInfo(int page);

	/**
	 * Clean up method which can be used to release resources, remove temp files etc.
	 * 
	 * @throws PdfRendererException Unchecked exception thrown in case of error. Refer to {@link PdfRendererException} for specific error codes.
	 */
	void dispose();

	/**
	 * Returns the memory footprint of the instantiated renderer.
	 * <p>
	 * The memory footprint provides an idea of how expensive this renderer instance is. Most likely the value denotes the
	 * number of bytes allocated. Note that the actual amount of used memory might be higher. Therefore the footprint should
	 * be regarded as relative measurement, useful for comparing renderer instance footprints.
	 * </p>
	 * 
	 * @return The memory footprint.
	 */
	long getMemoryFootprint();
	
	default void close() {
		dispose();
	}

}
