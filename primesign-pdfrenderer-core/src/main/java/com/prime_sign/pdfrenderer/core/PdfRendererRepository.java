package com.prime_sign.pdfrenderer.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.WillNotClose;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererException;

/**
 * Repository providing methods for creating and associating a pdf renderer with a certain identifier as well as
 * retrieving a previously created renderer (using the identifier) from the repository.
 * 
 * <p>
 * Note that {@code Cache*} annotations work only with interface-based proxies (which is the default). The fact that
 * Java annotations are not inherited from interfaces means that, class-based proxies
 * (<code>{@link org.springframework.cache.annotation.EnableCaching @EnableCaching}({@link org.springframework.cache.annotation.EnableCaching#proxyTargetClass() proxyTargetClass} =
 * true)</code>) or the weaving-based aspect
 * (<code>{@link org.springframework.cache.annotation.EnableCaching @EnableCaching}({@link org.springframework.cache.annotation.EnableCaching#mode() mode} = {@link org.springframework.context.annotation.AdviceMode#ASPECTJ AdviceMode.ASPECTJ})</code>)
 * are used, the caching settings are not recognized by the proxying and weaving infrastructure, and the object is not
 * wrapped in a caching proxy.
 * </p>
 * 
 * @see <a href="https://docs.spring.io/spring-framework/docs/5.3.6/reference/html/integration.html#cache">Spring
 *      Dokumentation: Enabling Caching Annotations</a>
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ParametersAreNonnullByDefault
@CacheConfig(cacheNames = "renderer")
public interface PdfRendererRepository {

	/**
	 * Creates a new pdf renderer instance for a provided identifier.
	 * 
	 * @param pdfDocument InputStream reflecting the pdf document (required; must not be {@code null}).
	 * @param id          The intended identifier for the renderer instance (required; must not be {@code null}).
	 * @return The newly created pdf renderer (never {@code null}).
	 * @throws IOException          Exception thrown in case of I/O error reading the provided pdf document.
	 * @throws PdfRendererException Unchecked exception thrown in case of error parsing the provided pdf document.
	 * @apiNote Providing an already used identifier replaces an existing renderer instance (both repository and cache).
	 * @apiNote Calling this method puts the created renderer into the cache (using the identifier as cache-key).
	 */
	@CachePut(key = "#id")
	@Nonnull
	PdfRenderer createRenderer(@WillNotClose InputStream pdfDocument, String id) throws IOException;

	/**
	 * Retrieves a previously created renderer instance from repository (or from cache).
	 * 
	 * @param id The identifier for the renderer instance (required; must not be {@code null}).
	 * @return A previously created pdf renderer (from repository or from cache) or an empty Optional (never {@code null}).
	 * @apiNote Calling this method returns the created renderer from the cache (using the identifier as cache-key) when
	 *          available.
	 */
	@Cacheable
	@Nonnull
	Optional<PdfRenderer> getRenderer(String id);

	/**
	 * Removes a certain renderer from the repository.
	 * 
	 * @param id The identifier for the renderer instance (required; must not be {@code null}).
	 * @apiNote The method does not provide a hint if the renderer was present or already deleted.
	 */
	@CacheEvict
	void removeRenderer(String id);

}
