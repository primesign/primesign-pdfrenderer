package com.prime_sign.pdfrenderer.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;

/**
 * Implementation of a {@link PdfRendererRepository}. Note that Spring caching needs to be in place for reasonable use
 * (considering caching annotation from the interface).
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
public class PdfRendererRepositoryImpl implements PdfRendererRepository {

	private final PdfRendererFactory pdfRendererFactory;

	public PdfRendererRepositoryImpl(@Nonnull PdfRendererFactory pdfRendererFactory) {
		this.pdfRendererFactory = Objects.requireNonNull(pdfRendererFactory, "'pdfRendererFactory' must not be null.");
	}

	@Override
	public PdfRenderer createRenderer(InputStream pdfDocument, String id) throws IOException {
		return pdfRendererFactory.createRenderer(pdfDocument);
	}

	@Override
	public Optional<PdfRenderer> getRenderer(String id) {
		// this repository implementation provided no CacheLoader (loading in demand) functionality
		return Optional.empty();
	}

	@Override
	public void removeRenderer(String id) {
		// this repository does not actually store renderers
	}

}
