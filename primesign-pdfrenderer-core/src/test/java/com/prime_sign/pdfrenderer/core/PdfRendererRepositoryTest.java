package com.prime_sign.pdfrenderer.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.same;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;

/**
 * Tests the behaviour of {@link PdfRendererRepository} without caching aspects.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
public class PdfRendererRepositoryTest {
	
	private IMocksControl ctrl = createControl();
	private PdfRendererFactory pdfRendererFactory = ctrl.createMock(PdfRendererFactory.class);
	private PdfRendererRepository cut = new PdfRendererRepositoryImpl(pdfRendererFactory);
	
	@Test
	void testCreateRenderer_expectThatRepositoryCallsFactory() throws IOException {

		ctrl.reset();
		
		InputStream in = new ByteArrayInputStream(new byte[] {});
		PdfRenderer pdfRenderer = ctrl.createMock(PdfRenderer.class);
		
		// expect that repository calls factory
		expect(pdfRendererFactory.createRenderer(in)).andReturn(pdfRenderer);
		
		ctrl.replay();
		
		PdfRenderer result = cut.createRenderer(in, "1234");
		
		ctrl.verify();
		
		assertThat(result, is(same(pdfRenderer)));
		
	}
	
	@Test
	void testGetRenderer_expectAlwaysEmpty() throws IOException {
		
		Optional<PdfRenderer> result = cut.getRenderer("1234");
		
		// expect that this specific implementation always returns empty optional (renderer is provided by caching which is not tested here)
		assertThat(result).isEmpty();
		
	}

}
