package com.prime_sign.pdfrenderer.rest.config;

import javax.annotation.Nonnull;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * Primary application configuration properties.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@Component
@ConfigurationProperties(prefix = "pdf-renderer", ignoreUnknownFields = false)
@Validated
public class PdfRendererConfigProperties {

	/**
	 * Specific repository configuration properties like cache duration and cache memory.
	 */
	@Valid
	@NestedConfigurationProperty
	@NotNull
	private RepositoryConfigProperties repository = new RepositoryConfigProperties();

	/**
	 * Returns the specific repository configuration properties.
	 * 
	 * @return The repository configuration properties (never {@code null}).
	 */
	@Nonnull
	public RepositoryConfigProperties getRepository() {
		return repository;
	}

}
