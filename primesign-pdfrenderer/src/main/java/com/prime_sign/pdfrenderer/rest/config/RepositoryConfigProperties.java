package com.prime_sign.pdfrenderer.rest.config;

import java.time.Duration;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import org.springframework.validation.annotation.Validated;

/**
 * Specific repository configuration properties.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ParametersAreNonnullByDefault
@Validated
public class RepositoryConfigProperties {

	/**
	 * Maximum memory (in bytes) dedicated to the repository (default: 256 MB).
	 */
	@Positive
	private long maximumMemory = 256 * 1024 * 1024; // 256 MB

	/**
	 * Amount of time the renderer is removed from cache after being accessed (default: 15 min).
	 */
	@NotNull
	private Duration expireAfterAccess = Duration.ofMinutes(15);

	/**
	 * Returns the maximum memory (in bytes) used for the repository (default: 256 MB).
	 * 
	 * @return The maximum memory (in bytes) used for the repository.
	 */
	public long getMaximumMemory() {
		return maximumMemory;
	}

	/**
	 * Sets the maximum memory (in bytes) used for the repository.
	 * 
	 * @param maximumMemory The maximum memory (in bytes) used for the repository.
	 */
	public void setMaximumMemory(long maximumMemory) {
		this.maximumMemory = maximumMemory;
	}

	/**
	 * Returns the amount of time the renderer is removed from cache after being accessed (default: 15 min).
	 * 
	 * @return The amount of idle time after that the renderer is removed (never {@code null}).
	 */
	@Nonnull
	public Duration getExpireAfterAccess() {
		return expireAfterAccess;
	}

	/**
	 * Sets the amount of time the renderer is removed from cache after being accessed.
	 * 
	 * @param expireAfterAccess The amount of idle time after that the renderer is removed (required; must not be
	 *                          {@code null}).
	 */
	public void setExpireAfterAccess(Duration expireAfterAccess) {
		this.expireAfterAccess = Objects.requireNonNull(expireAfterAccess, "'expireAfterAccess' must not be null.");
	}

	@Override
	public String toString() {
		return String.format("RepositoryConfigProperties [maximumMemory=%s, expireAfterAccess=%s]", maximumMemory, expireAfterAccess);
	}

}
