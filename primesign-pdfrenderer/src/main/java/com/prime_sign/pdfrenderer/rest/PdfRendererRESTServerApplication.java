package com.prime_sign.pdfrenderer.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class PdfRendererRESTServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfRendererRESTServerApplication.class, args);
	}
	
}
