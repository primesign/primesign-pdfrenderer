package com.prime_sign.pdfrenderer.rest.util;

import java.io.Closeable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.MDC;

/**
 * MDC helper class providing a single {@link #put(String, String)} method that returns a {@link Closeable} that can be
 * used in context of "<a href="https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">try-with-resources statement</a>"
 * similar to {@link MDC#putCloseable(String, String)}.
 * <p>
 * In contrast to the MDC.putCloseable(String, String) the method makes sure that the MDC is only removed
 * (resource is only closed) when it has not yet been created within this thread. This allows chained invocations like:
 * </p>
 * 
 * 
 * <pre>
 * try (MDCAutoCloseable outerCloseable = MDCAccessor.put("txId", "1234")) {
 * 
 *    // we should see txId '1234'
 *    System.out.println(MDC.get("txId"));
 * 
 *    try (MDCAutoCloseable innerCloseable = MDCAccessor.put("txId", "5678")) {
 * 
 *       // we should see txId '5678'
 *       System.out.println(MDC.get("txId"));
 * 
 *    }
 * 
 *    // we should see txId '1234'
 *    System.out.println(MDC.get("txId"));
 * 
 * }
 * 
 * // we should see txId null
 * System.out.println(MDC.get("txId"));
 * </pre>
 * 
 * <p>
 * Keep in mind that resources gets closed before execution of any catch or finally blocks.<br>
 * Refer to <a href="https://docs.oracle.com/javase/specs/jls/se8/html/jls-14.html#jls-14.20.3.2">The Java Language Specification
 * - 14.20.3.2. Extended try-with-resources</a> for further information.<br>
 * In case of catch or finally blocks where we would like to have the MDC of the try block the try-with-resources approach should not be used:</p>
 * <pre>
 * MDCAutoCloseable outerCloseable = MDCAccessor.put("txId", "1234");
 * try {
 * 
 *    // we should see txId '1234'
 *    System.out.println(MDC.get("txId"));
 * 
 *    try (MDCAutoCloseable innerCloseable = MDCAccessor.put("txId", "5678")) {
 * 
 *       // we should see txId '5678'
 *       System.out.println(MDC.get("txId"));
 * 
 *    }
 * 
 *    // we should see txId '1234'
 *    System.out.println(MDC.get("txId"));
 * 
 *    if (System.currentTimeMillis() &gt; 0) {
 *       throw new Exception("Test");
 *    }
 * 
 * } catch (Exception e) {
 * 
 *    // we should see txId '1234'
 *    System.out.println(MDC.get("txId"));
 * 
 * } finally {
 *    outerCloseable.close();
 * }
 * 
 * // we should see txId null
 * System.out.println(MDC.get("txId"));
 * </pre>
 * 
 * @author Thomas Knall, PrimeSign GmbH
 * @see MDC#putCloseable(String, String)
 * @see <a href="https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">try-with-resources
 *      tutorial</a>
 *
 */
public class MDCAccessor {

	/**
	 * AutoCloseable that - on {@link #close()} - restores the previous MDC value from the current thread (if is has
	 * already been set when {@link MDCAccessor#put(String, String)} has been invoked) or removes the MDC value
	 * otherwise.
	 */
	public interface MDCAutoCloseable extends AutoCloseable {
		
		/**
		 * Cleares the underlying MDC.
		 */
		void close();

		/**
		 * Returns the value associated with this AutoCloseable.
		 * 
		 * @return The value (may be {@code null}).
		 * @implNote This value is not cleared when {@link #close()} is called meaning that {@link #getValue()} still
		 *           returns the originally provided value.
		 */
		@Nullable
		String getValue();

	}

	/**
	 * Puts the provided value into MDC with key "{@code key}" and returns an {@link AutoCloseable} that can be used for
	 * "<a href=
	 * "https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">try-with-resources</a>".
	 * <p>
	 * When calling {@link MDCAutoCloseable#close()} (e.g. implicitely after execution of the code enclosed by
	 * "try-with-resources") a previously set value for MDC "{@code key}" will be restored (if any) or the MDC will be
	 * removed.
	 * </p>
	 * <p>
	 * In case of {@code value} is {@code null} a currently set MDC with key "{@code key}" is removed until being
	 * restored by calling {@link MDCAutoCloseable#close()}.
	 * </p>
	 * 
	 * @param key   The key (required; must not be {@code null}).
	 * @param value The value (optional; may be {@code null}). If {@code null} a currently set MDC with key
	 *              "{@code key}" is removed.
	 * @return An {@link AutoCloseable} (never {@code null}).
	 */
	@Nonnull
	public static MDCAutoCloseable put(@Nonnull final String key, @Nullable final String value) {

		final String oldValue = MDC.get(key);
		
		if (value != null) {
			MDC.put(key, value);
		} else {
			MDC.remove(key);
		}

		if (oldValue == null) {

			return new MDCAutoCloseable() {
				@Override
				public void close() {

					MDC.remove(key);
				}

				@Override
				public String getValue() {
					return value;
				}
			};

		} else {

			return new MDCAutoCloseable() {
				@Override
				public void close() {
					MDC.put(key, oldValue);
				}

				@Override
				public String getValue() {
					return value;
				}
			};

		}
	}
	
	/**
	 * This method puts a provided {@code value} into MDC with key "{@code key}" <strong> only if {@code value} is not
	 * {@code null}</strong>, otherwise this method does nothing. In both cases it returns an {@link AutoCloseable} that
	 * can be used for "<a href=
	 * "https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">try-with-resources</a>".
	 * <p>
	 * In case of {@code value} not {@code null} this method behaves like {@link #put(String, String)}. Refer to that
	 * method for further documentation.
	 * </p>
	 * <p>
	 * This method is a shortcut for
	 * </p>
	 * <pre>
	 * if (value != null) {
	 * 
	 *    return put(key, value);
	 * 
	 * } else {
	 * 
	 *    return new MDCAutoCloseable() {
	 * 
	 *       &#64;Override
	 *       public void close() {
	 *       }
	 * 
	 *       &#64;Override
	 *       public String getValue() {
	 *          return value;
	 *       }
	 * 
	 *    };
	 * 
	 * }
	 * </pre>
	 * 
	 * @param key   The key (required; must not be {@code null}).
	 * @param value The value (optional; may be {@code null}).
	 * @return An {@link AutoCloseable} (never {@code null}).
	 */
	@Nonnull
	public static MDCAutoCloseable putIfPresent(@Nonnull String key, @Nullable final String value) {
		
		if (value != null) {
			
			return put(key, value);
			
		} else {
			
			// do not put anything into MDC... keep state as-is
			
			return new MDCAutoCloseable() {
				
				@Override
				public void close() {
					// nothing to do
				}

				@Override
				public String getValue() {
					return value;
				}
				
			};
			
		}
		
	}

}
