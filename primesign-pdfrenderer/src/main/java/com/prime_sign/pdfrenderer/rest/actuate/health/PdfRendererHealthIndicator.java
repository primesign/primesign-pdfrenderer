package com.prime_sign.pdfrenderer.rest.actuate.health;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;

/**
 * Spring Actuator health indicator testing the pdf renderer: initializing a renderer for a very small document using
 * the software stack, querying page info, rendering a page and disposing the renderer.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 * @see <a href=
 *      "https://docs.spring.io/spring-boot/docs/3.0.5/reference/htmlsingle/#actuator.endpoints.health.writing-custom-health-indicators">Writing
 *      Custom HealthIndicators</a>
 * @implNote The identifier for a given HealthIndicator is the name of the bean without the HealthIndicator suffix.
 * @implNote Spring Boot will log a warning message for any health indicator that takes longer than 10 seconds to
 *           respond.
 */
@Component
public class PdfRendererHealthIndicator implements HealthIndicator {
	
	// @formatter:off
	private final byte[] emptyPdf = Base64.getDecoder().decode(
			"JVBERi0xLjQKJfbk/N8KMSAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovVmVyc2lvbiAvMS40Ci9Q" +
			"YWdlcyAyIDAgUgo+PgplbmRvYmoKMiAwIG9iago8PAovVHlwZSAvUGFnZXMKL0tpZHMgWzMgMCBS" +
			"XQovQ291bnQgMQo+PgplbmRvYmoKMyAwIG9iago8PAovVHlwZSAvUGFnZQovTWVkaWFCb3ggWzAu" +
			"MCAwLjAgNTk1LjI3NTYzIDg0MS44ODk4XQovUGFyZW50IDIgMCBSCj4+CmVuZG9iagp4cmVmCjAg" +
			"NAowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDAwMTUgMDAwMDAgbg0KMDAwMDAwMDA3OCAwMDAw" +
			"MCBuDQowMDAwMDAwMTM1IDAwMDAwIG4NCnRyYWlsZXIKPDwKL1Jvb3QgMSAwIFIKL0lEIFs8QUUx" +
			"MkUyNENEN0NBQzQzMzk5QjFBNzUzNDNCMTU2Mzg+IDxBRTEyRTI0Q0Q3Q0FDNDMzOTlCMUE3NTM0" +
			"M0IxNTYzOD5dCi9TaXplIDQKPj4Kc3RhcnR4cmVmCjIyMQolJUVPRgo=");
	// @formatter:on

	public PdfRendererHealthIndicator(@Nonnull PdfRendererFactory pdfRendererFactory) {
		this.pdfRendererFactory = Objects.requireNonNull(pdfRendererFactory, "'pdfRendererFactory' must not be null.");
	}

	private final PdfRendererFactory pdfRendererFactory;
	
	private final Logger log = LoggerFactory.getLogger(PdfRendererHealthIndicator.class);

	@Override
	public Health health() {

		try {
			check();
		} catch (Throwable e) { // NOSONAR (cover OutOfMemoryError etc.)
			log.warn("Unable to render test document.", e);
			return Health.outOfService().withException(e).build();
		}

		return Health.up().build();
	}

	/**
	 * Initializes a renderer with a very small document using the software stack, queries page info, renders a page and
	 * finally disposes the renderer.
	 * 
	 * @throws IOException In case the pdf renderer could not be initialized (should never occur with the embedded
	 *                     document).
	 */
	private void check() throws IOException {

		try (InputStream in = new ByteArrayInputStream(emptyPdf); PdfRenderer renderer = pdfRendererFactory.createRenderer(in)) {

			renderer.getPageCount();
			renderer.getMemoryFootprint();
			renderer.getPageInfo(1);
			renderer.renderPage(1, 1.0f);

		}

	}

}
