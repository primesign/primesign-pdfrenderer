package com.prime_sign.pdfrenderer.rest;

import static com.prime_sign.pdfrenderer.rest.PdfRenderingConfiguration.MDC_RENDERER;

import java.util.concurrent.ScheduledThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Scheduler;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;
import com.prime_sign.pdfrenderer.core.PdfRendererRepository;
import com.prime_sign.pdfrenderer.core.PdfRendererRepositoryImpl;
import com.prime_sign.pdfrenderer.rest.config.PdfRendererConfigProperties;
import com.prime_sign.pdfrenderer.rest.util.MDCAccessor;
import com.prime_sign.pdfrenderer.rest.util.MDCAccessor.MDCAutoCloseable;

/**
 * Configures the underlying {@link PdfRendererRepository} including (caffeine) caching.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@Configuration
@EnableCaching
public class RepositoryConfiguration {

	private Logger log = LoggerFactory.getLogger(RepositoryConfiguration.class);

	@Bean
	PdfRendererRepository pdfRendererRepository(PdfRendererFactory pdfRendererFactory) {
		return new PdfRendererRepositoryImpl(pdfRendererFactory);
	}

	@Bean(destroyMethod = "shutdown")
	ScheduledThreadPoolExecutor scheduledThreadPoolExecutor() {
		return new ScheduledThreadPoolExecutor(1);
	}

	@Bean
	Caffeine<?, ?> caffeineBuilder(PdfRendererConfigProperties cfg) {
		log.info("Configuring repository cache: {}", cfg.getRepository());
		// @formatter:off
		return Caffeine.newBuilder()
				.expireAfterAccess(cfg.getRepository().getExpireAfterAccess())
				.maximumWeight(cfg.getRepository().getMaximumMemory())
				.removalListener((key, cachedObject, cause) -> {
					if (cachedObject instanceof PdfRenderer) {
						try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, String.valueOf(key))) {
							log.info("Pdf renderer '{}' removed: {} ({}).", key, cause, cachedObject);
						}
					}
				})
				.weigher((key, value) -> {
					if (value instanceof PdfRenderer pdfRenderer) {
						long memoryFootprint = pdfRenderer.getMemoryFootprint();
						return memoryFootprint > Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) memoryFootprint;
					}
					return 0;
				})
				.scheduler(Scheduler.forScheduledExecutorService(scheduledThreadPoolExecutor()));
		// @formatter:on
	}

}
