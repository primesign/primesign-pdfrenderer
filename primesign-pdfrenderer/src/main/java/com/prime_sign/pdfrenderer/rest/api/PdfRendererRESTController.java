package com.prime_sign.pdfrenderer.rest.api;

import static com.prime_sign.pdfrenderer.rest.PdfRenderingConfiguration.MDC_RENDERER;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.prime_sign.pdfrenderer.api.PageInfo;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.core.PdfRendererRepository;
import com.prime_sign.pdfrenderer.rest.util.MDCAccessor;
import com.prime_sign.pdfrenderer.rest.util.MDCAccessor.MDCAutoCloseable;

/**
 * Reflects the REST api.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@RestController
@ParametersAreNonnullByDefault
public class PdfRendererRESTController {

	private Logger log = LoggerFactory.getLogger(PdfRendererRESTController.class);
	
	private final PdfRendererRepository pdfRendererRepository;
	
	public PdfRendererRESTController(PdfRendererRepository pdfRendererRepository) {
		this.pdfRendererRepository = Objects.requireNonNull(pdfRendererRepository, "'pdfRendererRepository' must not be null.");
	}

	@PostMapping(path = "/{id}", consumes = MediaType.APPLICATION_PDF_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void createRenderer(InputStream pdfDocument, @PathVariable String id) throws IOException {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			log.debug("Create pdf renderer with id '{}'.", id);
			PdfRenderer pdfRenderer = pdfRendererRepository.createRenderer(pdfDocument, id);
			log.info("Created pdf renderer with id '{}' ({}).", id, pdfRenderer);
		}
	}
	
	@DeleteMapping("/{id}")
	public void removeRenderer(InputStream pdfDocument, @PathVariable String id) throws IOException {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			log.debug("Remove pdf renderer with id '{}'.", id);
			pdfRendererRepository.removeRenderer(id);
			log.info("Removed pdf renderer with id '{}'.", id);
		}
	}

	@GetMapping(path = "/{id}/{page}/{zoom}.png", produces = MediaType.IMAGE_PNG_VALUE)
	@Nonnull
	public BufferedImage getPage(@PathVariable String id, @PathVariable int page, @PathVariable float zoom) {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			log.debug("Get rendered image for id '{}', page {}, zoom {}.", id, page, zoom);
			return pdfRendererRepository.getRenderer(id)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
					.renderPage(page, zoom);
		}
	}
	
	@GetMapping(path = "/{id}/{page}.png", produces = MediaType.IMAGE_PNG_VALUE)
	@Nonnull
	public BufferedImage getPage(@PathVariable String id, @PathVariable int page) {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			return getPage(id, page, 1.0f);
		}
	}
	
	@GetMapping(path = "/{id}/pageInfo/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
	@Nonnull
	public PageInfo getPageInfo(@PathVariable String id, @PathVariable int page) {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			log.debug("Get pageInfo for id '{}', page {}.", id, page);
			return pdfRendererRepository.getRenderer(id)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
					.getPageInfo(page);
		}
	}
	
	@GetMapping(path = "/{id}/pageCount", produces = MediaType.APPLICATION_JSON_VALUE)
	public int getPageCount(@PathVariable String id) {
		try (MDCAutoCloseable mdcAutoCloseable = MDCAccessor.put(MDC_RENDERER, id)) {
			log.debug("Get pageCount for id '{}'.", id);
			return pdfRendererRepository.getRenderer(id)
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
					.getPageCount();
		}
	}

}
