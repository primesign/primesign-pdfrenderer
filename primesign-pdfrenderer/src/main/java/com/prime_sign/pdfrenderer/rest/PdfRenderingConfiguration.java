package com.prime_sign.pdfrenderer.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prime_sign.pdfrenderer.api.PdfRendererFactory;
import com.prime_sign.pdfrenderer.impl.mupdf.MuPdfRendererFactory;

/**
 * Configures the underlying pdf renderer.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@Configuration
public class PdfRenderingConfiguration {
	
	public static final String MDC_RENDERER = "rendererId";
	
	@Bean
	PdfRendererFactory pdfRendererFactory() {
		return new MuPdfRendererFactory();
	}

}
