package com.prime_sign.pdfrenderer.rest.api;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.prime_sign.pdfrenderer.api.PageInfo;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.core.PdfRendererRepository;

/**
 * MVC tests for the REST controller {@link PdfRendererRESTController}.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@SpringJUnitWebConfig
class PdfRendererRESTControllerTest {

	@Autowired
	private PdfRendererRESTController cut;

	@Autowired
	private PdfRendererRepository pdfRendererRepository;

	@Configuration
	@EnableWebMvc
	static class Config implements WebMvcConfigurer {

		@Bean
		PdfRendererRESTController cut(PdfRendererRepository pdfRendererRepository) {
			return new PdfRendererRESTController(pdfRendererRepository);
		}

		@Bean
		PdfRendererRepository pdfRendererRepository() {
			return control.createMock(PdfRendererRepository.class);
		}

	}

	private static final IMocksControl control = createControl();
	private MockMvc mockMvc;

	@BeforeEach
	void beforeEach() throws Exception {
		
		// @formatter:off
		mockMvc = MockMvcBuilders.standaloneSetup(cut)
					.setMessageConverters(
						new BufferedImageHttpMessageConverter(),
						new MappingJackson2HttpMessageConverter())
					.alwaysDo(
						print())
					.build();
		// @formatter:on
		
		control.reset();
	}

	@Test
	void testCreateRenderer() throws Exception {
		
		PdfRenderer pdfRenderer = control.createMock(PdfRenderer.class);

		Capture<InputStream> inputStream = newCapture();
		expect(pdfRendererRepository.createRenderer(capture(inputStream), eq("12345"))).andReturn(pdfRenderer);

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				post("/12345")
					.contentType(MediaType.APPLICATION_PDF_VALUE)
					.content("pdf".getBytes()))
			.andExpect(
				status().isCreated());
		// @formatter:on
		
		byte[] providedToRendererRepository = IOUtils.toByteArray(inputStream.getValue());
		assertThat(providedToRendererRepository, is("pdf".getBytes()));

		control.verify();
	}

	@Test
	void testCreateRendererWrongContentType() throws Exception {

		control.replay();
		
		// @formatter:off
		mockMvc
			.perform(
				post("/12345")
					.contentType(MediaType.APPLICATION_JSON)
					.accept("application/json")
					.content("pdf".getBytes()))
			.andExpect(
				status().isUnsupportedMediaType());
		// @formatter:on
		
		control.verify();

	}

	@Test
	void testDeleteRenderer() throws Exception {

		pdfRendererRepository.removeRenderer("12345");

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				delete("/12345")
					.content("pdf".getBytes()))
			.andExpect(
				status().isOk());
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageAndZoom() throws Exception {
		
		PdfRenderer pdfRenderer = control.createMock(PdfRenderer.class);
		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);

		// prepare expected png from image
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		byte[] png = out.toByteArray();
		
		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.of(pdfRenderer));
		expect(pdfRenderer.renderPage(1, 2f)).andReturn(image);

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/1/2.png"))
			.andExpect(
				status().isOk())
			.andExpect(
				content().contentType(MediaType.IMAGE_PNG_VALUE))
			.andExpect(
				content().bytes(png));
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageAndZoomRendererNotFound() throws Exception {

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.empty());

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/1/2.png"))
			.andExpect(
				status().isNotFound());
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPage() throws Exception {

		PdfRenderer pdfRenderer = control.createMock(PdfRenderer.class);
		BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);

		// prepare expected png from image
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		byte[] png = out.toByteArray();

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.of(pdfRenderer));
		expect(pdfRenderer.renderPage(1, 1f)).andReturn(image);

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/1.png"))
			.andExpect(
				status().isOk())
			.andExpect(
				content().contentType(MediaType.IMAGE_PNG_VALUE))
			.andExpect(
				content().bytes(png));
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageRendererNotFound() throws Exception {

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.empty());

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/1.png"))
			.andExpect(
				status().isNotFound());
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageInfo() throws Exception {
		
		PdfRenderer pdfRenderer = control.createMock(PdfRenderer.class);
		PageInfo pageInfo = new PageInfo();
		pageInfo.setHeight(2);
		pageInfo.setWidth(3);

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.of(pdfRenderer));
		expect(pdfRenderer.getPageInfo(EasyMock.eq(1))).andReturn(pageInfo);

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/pageInfo/1"))
			.andExpect(
				status().isOk())
			.andExpect(
				content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(
				jsonPath("$.height").value("2.0"))
			.andExpect(
				jsonPath("$.width").value("3.0"));
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageInfoRendererNotFound() throws Exception {

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.empty());

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/pageInfo/1"))
			.andExpect(
				status().isNotFound());
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageCount() throws Exception {
		
		PdfRenderer pdfRenderer = control.createMock(PdfRenderer.class);

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.of(pdfRenderer));
		expect(pdfRenderer.getPageCount()).andReturn(2);

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/pageCount"))
			.andExpect(
				status().isOk())
			.andExpect(
				content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(
				content().json("2"));
		// @formatter:on

		control.verify();
	}

	@Test
	void testGetPageCountRendererNotFound() throws Exception {

		expect(pdfRendererRepository.getRenderer("12345")).andReturn(Optional.empty());

		control.replay();

		// @formatter:off
		mockMvc
			.perform(
				get("/12345/pageCount"))
			.andExpect(
				status().isNotFound());
		// @formatter:on

		control.verify();
	}

}
