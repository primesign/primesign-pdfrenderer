package com.prime_sign.pdfrenderer.rest.util;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

import com.prime_sign.pdfrenderer.rest.util.MDCAccessor.MDCAutoCloseable;

/**
 * Tests for {@link MDCAccessor}.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 */
public class MDCAccessorTest {

	@Test
	public void testPut() {

		MDCAutoCloseable outerCloseable = MDCAccessor.put("txId", "outermost");

		try {

			assertThat(MDC.get("txId"), is("outermost"));

			try (MDCAutoCloseable innerCloseable = MDCAccessor.put("txId", "inner")) {

				assertThat(MDC.get("txId"), is("inner"));

				try (MDCAutoCloseable innerMostCloseable = MDCAccessor.put("txId", "innermost")) {

					assertThat(MDC.get("txId"), is("innermost"));

				}

				assertThat(MDC.get("txId"), is("inner"));

			}

			// we should see txId '1234'
			assertThat(MDC.get("txId"), is("outermost"));

			if (System.currentTimeMillis() > 0) {
				throw new Exception("Test");
			}

		} catch (Exception e) {

			// we should see txId '1234'
			assertThat(MDC.get("txId"), is("outermost"));

		} finally {

            assertThat(MDC.get("txId"), is("outermost"));
            outerCloseable.close();

		}

		assertThat(MDC.get("txId"), is(nullValue()));

	}

	@Test
	public void testPutGetValue() {

		MDCAutoCloseable closeable = MDCAccessor.put("txId", "value");

		try {

			assertThat(closeable.getValue(), is("value"));

		} finally {

			assertThat(closeable.getValue(), is("value"));

			closeable.close();

			// expecting stil returning original value after close()
			assertThat(closeable.getValue(), is("value"));

		}

	}

	@Test
	public void testPutIfPresent() {

		try (MDCAutoCloseable closeable = MDCAccessor.putIfPresent("txId", "value")) {

			assertThat(closeable.getValue(), is("value"));
			assertThat(MDC.get("txId"), is("value"));

		}

		assertThat(MDC.get("txId"), is(nullValue()));

		try (MDCAutoCloseable closeable = MDCAccessor.putIfPresent("txId", null)) {

			assertThat(closeable.getValue(), is(nullValue()));
			assertThat(MDC.get("txId"), is(nullValue()));

		}

	}

}
