package com.prime_sign.pdfrenderer.rest.actuate.health;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.expect;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.awt.image.BufferedImage;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.prime_sign.pdfrenderer.api.PageInfo;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;

/**
 * Tests that a health check is enabled and will try to render a pdf document.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class ActuatorTest {

	@TestConfiguration
	static class Config {

		@Bean
		@Primary
		PdfRendererFactory testPdfRendererFactory() {
			return ctrl.createMock(PdfRendererFactory.class);
		}

	}

	private static final IMocksControl ctrl = EasyMock.createControl();

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private PdfRendererFactory pdfRendererFactory;

	@BeforeEach
	void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	void test_Actuator_Health_ok() throws Exception {

		ctrl.reset();

		PdfRenderer pdfRenderer = ctrl.createMock(PdfRenderer.class);

		expect(pdfRendererFactory.createRenderer(anyObject())).andReturn(pdfRenderer);

		expect(pdfRenderer.getPageCount()).andReturn(1);
		expect(pdfRenderer.getMemoryFootprint()).andReturn(42L);
		expect(pdfRenderer.getPageInfo(1)).andReturn(new PageInfo());
		expect(pdfRenderer.renderPage(1, 1.0f)).andReturn(new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB));

		pdfRenderer.close();

		ctrl.replay();

		// @formatter:off
		mockMvc
			.perform(get("/actuator/health"))
			.andExpect(status().isOk());
		// @formatter:on

		ctrl.verify();

	}

	@Test
	void test_Actuator_Health_nok() throws Exception {

		ctrl.reset();

		expect(pdfRendererFactory.createRenderer(anyObject())).andThrow(new RuntimeException("Failed initializing renderer."));

		ctrl.replay();

		// @formatter:off
		mockMvc
			.perform(get("/actuator/health"))
			.andExpect(status().isServiceUnavailable());
		// @formatter:on

		ctrl.verify();

	}

}
