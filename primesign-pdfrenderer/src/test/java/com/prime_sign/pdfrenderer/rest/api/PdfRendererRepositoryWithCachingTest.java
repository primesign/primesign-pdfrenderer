package com.prime_sign.pdfrenderer.rest.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;
import com.prime_sign.pdfrenderer.core.PdfRendererRepository;
import com.prime_sign.pdfrenderer.core.PdfRendererRepositoryImpl;

/**
 * This class tests caching aspects of {@link PdfRendererRepository}.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@SpringJUnitConfig
@EnableCaching
public class PdfRendererRepositoryWithCachingTest {

	@Configuration
	static class Config {

		@Bean
		PdfRendererFactory pdfRendererFactory() {
			return ctrl().createMock(PdfRendererFactory.class);
		}

		@Bean
		IMocksControl ctrl() {
			return createControl();
		}

		@Bean
		PdfRendererRepository cut() {
			return new PdfRendererRepositoryImpl(pdfRendererFactory());
		}

		@Bean
		CacheManager cacheManager() {
			SimpleCacheManager cacheManager = new SimpleCacheManager();
			List<Cache> caches = new ArrayList<>();
			caches.add(new ConcurrentMapCache("renderer"));
			cacheManager.setCaches(caches);
			return cacheManager;
		}

	}

	@Autowired
	private IMocksControl ctrl;

	@Autowired
	private PdfRendererRepository cut;
	
	@Autowired
	private PdfRendererFactory pdfRendererFactory;

	@Test
	void testGetRendererWithoutPrecedentCreate_expectEmptyResult() {

		ctrl.reset();

		ctrl.replay();

		assertThat(cut.getRenderer("1234")).isEmpty(); // expect that no renderer is available

		ctrl.verify();

	}

	@Test
	void testGetRendererWithPrecedentCreate() throws IOException {

		ctrl.reset();

		InputStream in = new ByteArrayInputStream(new byte[] {});
		PdfRenderer pdfRenderer = ctrl.createMock(PdfRenderer.class);
		
		expect(pdfRendererFactory.createRenderer(in)).andReturn(pdfRenderer);
		
		ctrl.replay();

		// get -> 404
		assertThat(cut.getRenderer("1234")).isEmpty(); // expect that no renderer is available
		// post -> 201
		cut.createRenderer(in, "1234");
		// get -> 200
		assertThat(cut.getRenderer("1234")).contains(pdfRenderer);

		ctrl.verify();

	}

	@Test
	void testGetRendererAfterEviction_expectEmptyResult() throws IOException {

		ctrl.reset();

		InputStream in = new ByteArrayInputStream(new byte[] {});
		PdfRenderer pdfRenderer = ctrl.createMock(PdfRenderer.class);
		
		expect(pdfRendererFactory.createRenderer(in)).andReturn(pdfRenderer);
		
		ctrl.replay();

		// post -> 201
		cut.createRenderer(in, "1234");
		// get -> 200
		assertThat(cut.getRenderer("1234")).contains(pdfRenderer);
		// delete -> 200
		cut.removeRenderer("1234");
		// get -> 404
		assertThat(cut.getRenderer("1234")).isEmpty(); // expect that renderer is no longer available

		ctrl.verify();

	}

}
