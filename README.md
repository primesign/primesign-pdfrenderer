# primesign-pdfrenderer

PrimeSign PdfRenderer reflects a Spring Boot application providing a REST api for pagewise rendering of PDF documents.

## License

PrimeSign PdfRenderer is provided under the [GNU Affero General Public License](https://www.gnu.org/licenses/agpl-3.0.html) (AGPL).

## How to build

### Requirements

The following components are required to build the project:
* [Apache Maven](https://maven.apache.org/) (built and tested with version 3.9.6)
* JDK 21 (built and tested with OpenJDK 21.0.3 (Eclipse Adoptium))

Now you can build PrimeSign PdfRenderer with:
```shell
mvn clean install
```

## How to run

### Requirements

The following components are required to run the Spring Boot application:
* Java 21 (tested with OpenJDK Runtime Environment Temurin-21.0.5+11 (build 21.0.5+11-LTS))
* OS-specific binary build of [Artifex MuPDF](https://mupdf.com/) (tested with version 1.24.1)

First provide the native library suitable for your intended runtime environment (e.g. use a separate folder `./native-libs`).

Make sure to name the library as follows:
* `libmupdf-1.25.2-linux64.so -> libmupdf_java.so`
* `libmupdf-1.25.2-windows64.dll -> mupdf_java.dll`
* `libmupdf-1.25.2-macos64.dylib -> libmupdf_java.dylib`

Run PrimeSign PdfRenderer:
```shell
cd ./primesign-pdfrenderer/target
java -jar -Djava.library.path=./native-libs primesign-pdfrenderer-1.25.2.jar
```

### How to use

Running with default configuration PrimeSign PdfRenderer REST api listens at: `http://localhost:8080/`.

Look at the provided Postman collection for further information about the API.

## Monitoring

By default the following **health endpoint** is available:

http://localhost:8080/actuator/health

In order to enable further **Prometheus** metrics, the following environment variable can be set:

````
MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE = health, prometheus
````

This activates the following endpoint in addition to the above-mentioned health endpoint:

http://localhost:8080/actuator/prometheus

Note that the management API can be set to its own **port** or server **address** (see also [Spring Boot - Monitoring and Management Over HTTP](https://docs.spring.io/spring-boot/reference/actuator/monitoring.html)):

````
MANAGEMENT_SERVER_PORT = 8081
MANAGEMENT_SERVER_ADDRESS = 127.0.0.1
````
