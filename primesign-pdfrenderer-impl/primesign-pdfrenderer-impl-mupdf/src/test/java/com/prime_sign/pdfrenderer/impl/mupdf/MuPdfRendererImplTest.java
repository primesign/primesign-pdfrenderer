package com.prime_sign.pdfrenderer.impl.mupdf;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.condition.OS.LINUX;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.input.CountingInputStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledOnOs;

import com.prime_sign.pdfrenderer.api.PageInfo;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererException;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;


/**
 * Tests for native renderer MuPdf.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@EnabledOnOs(LINUX)
public class MuPdfRendererImplTest {

	private final PdfRendererFactory pdfRendererFactory = new MuPdfRendererFactory();

	@Test
	void testRenderPage() throws Exception {

		try (InputStream pdf = MuPdfRendererImplTest.class.getResourceAsStream("blindtext.pdf");
				PdfRenderer pdfRenderer = pdfRendererFactory.createRenderer(pdf)) {
			
			BufferedImage renderedPage = pdfRenderer.renderPage(1, 1.0f);
			assertNotNull(renderedPage);
			assertEquals(595, renderedPage.getWidth());
			assertEquals(841, renderedPage.getHeight());
			
		}
		
	}

	@Test
	void testGetMemoryFootprint() throws Exception {
		
		try (CountingInputStream in = new CountingInputStream(MuPdfRendererImplTest.class.getResourceAsStream("blindtext.pdf"));
				PdfRenderer pdfRenderer = pdfRendererFactory.createRenderer(in)) {
			
			long memoryFootprint = pdfRenderer.getMemoryFootprint();
			assertEquals(in.getByteCount(), memoryFootprint);
			
		}
		
	}

	@Test
	void testGetPageInfo() throws Exception {
		
		try (InputStream pdf = MuPdfRendererImplTest.class.getResourceAsStream("blindtext.pdf");
				PdfRenderer pdfRenderer = pdfRendererFactory.createRenderer(pdf)) {
			
			PageInfo pageInfo = pdfRenderer.getPageInfo(1);
			assertEquals(595.32f, pageInfo.getWidth(), "width");
			assertEquals(841.92f, pageInfo.getHeight(), "height");
			
		}
		
	}

	@Test
	void testGetPageCount() throws Exception {
		
		try (InputStream pdf = MuPdfRendererImplTest.class.getResourceAsStream("blindtext.pdf");
				PdfRenderer pdfRenderer = pdfRendererFactory.createRenderer(pdf)) {

			int pageCount = pdfRenderer.getPageCount();
			assertEquals(74, pageCount);

		}
		
	}
	@Test
	void testMultiThreading() throws PdfRendererException, IOException, InterruptedException {

		ExecutorService pool = Executors.newFixedThreadPool(5);

		try {
			
			final PdfRenderer pdfRenderer;
			try (InputStream in = MuPdfRendererImplTest.class.getResourceAsStream("blindtext.pdf")) {
				pdfRenderer = pdfRendererFactory.createRenderer(in);
			}
			for (int i = 0; i < 5; i++) {		
				pool.submit(new RenderingTask(pdfRenderer, (int)(Math.random() * (10))));
			}		
			pool.shutdown();
			
			if (!pool.awaitTermination(300, TimeUnit.SECONDS)) {
				// cancel currently executing tasks
				pool.shutdownNow();
				// wait a while for tasks to respond to being cancelled
				pool.awaitTermination(60, TimeUnit.SECONDS);
				fail("Timeout. Thread pool did not terminate properly.");
			}
		} finally {
			if (!pool.isShutdown()) {
				pool.shutdownNow();
				// wait a while for tasks to respond to being cancelled
				boolean terminated = pool.awaitTermination(60, TimeUnit.SECONDS);
				if (!terminated) {
					fail("Timeout. Thread pool did not terminate properly.");
				}
			}
		}

	}
	
	private static class RenderingTask implements Runnable {
		
		private PdfRenderer pdfRenderer;
		private int maxPages = -1;

		public RenderingTask(PdfRenderer pdfRenderer, int maxPages) {
			this.pdfRenderer = pdfRenderer;
			this.maxPages = maxPages;
		}

		@Override
		public void run() {
			try {
				int pageCount = pdfRenderer.getPageCount();

				for (int page = 1; page <= maxPages; page++) {
					if (page > pageCount) {
						page = 1;
					}
					int randomPage = (int) (Math.random() * (pageCount) + 1);

					BufferedImage img = pdfRenderer.renderPage(randomPage, 1);
					assertNotNull(img, "PdfRenderer did not return a rendered image.");
				}
			} catch (Exception e) {
				fail(e.getMessage());
			}
		}

	}

}
