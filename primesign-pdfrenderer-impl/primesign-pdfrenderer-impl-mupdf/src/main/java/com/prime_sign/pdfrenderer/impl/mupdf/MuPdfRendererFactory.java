package com.prime_sign.pdfrenderer.impl.mupdf;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererFactory;

/**
 * Factory creating MuPdf renderer instances.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
public class MuPdfRendererFactory implements PdfRendererFactory {

	@Override
	public PdfRenderer createRenderer(InputStream pdfDocument) throws IOException {
		return new MuPdfRendererImpl(IOUtils.toByteArray(pdfDocument));
	}

}
