package com.prime_sign.pdfrenderer.impl.mupdf;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Objects;

import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.artifex.mupdf.fitz.ColorSpace;
import com.artifex.mupdf.fitz.DisplayList;
import com.artifex.mupdf.fitz.Document;
import com.artifex.mupdf.fitz.DrawDevice;
import com.artifex.mupdf.fitz.Matrix;
import com.artifex.mupdf.fitz.Page;
import com.artifex.mupdf.fitz.Pixmap;
import com.artifex.mupdf.fitz.Rect;
import com.prime_sign.pdfrenderer.api.PageInfo;
import com.prime_sign.pdfrenderer.api.PdfRenderer;
import com.prime_sign.pdfrenderer.api.PdfRendererException;

/**
 * Renderer implementation for Artifex MuPdf.
 * 
 * @author Thomas Knall, PrimeSign GmbH
 *
 */
@ParametersAreNonnullByDefault
public class MuPdfRendererImpl implements PdfRenderer {

	private final Logger log = LoggerFactory.getLogger(MuPdfRendererImpl.class);

	private Document document;
	private final int pageCount;
	private final long memoryFootprint;

	public MuPdfRendererImpl(byte[] buffer) {
		Objects.requireNonNull(buffer);
		memoryFootprint = buffer.length;
		log.debug("Initializing pdf renderer (memoryFootprint={}).", FileUtils.byteCountToDisplaySize(buffer.length));
		document = Document.openDocument(buffer, "pdf");
		log.trace("Renderer successfully initialized with pdf document.");
		pageCount = document.countPages();
		if (pageCount < 0) {
			throw new IllegalStateException("Renderer returns invalid page count (" + pageCount + ").");
		}
	}

	@Override
	public BufferedImage renderPage(int pageNum, float zoom) throws PdfRendererException {

		if (pageNum < 1 || pageNum > getPageCount()) {
			throw new PdfRendererException(
					PdfRendererException.ERROR_INVALID_PAGE_NO,
					"Unable to render page no " + pageNum + ". Must be within [1," + getPageCount() + "]."
					);
		}

		log.debug("Rendering document page {} with zoom {}", pageNum, zoom);

		DisplayList displayList;
		Rect bounds;
		// Access to the doc/page needs to be synchronized but a DisplayList can then be used freely
		synchronized (this) {
			Page page = document.loadPage(pageNum - 1);
			try {
				bounds = page.getBounds();
				displayList = page.toDisplayList();
			} finally { // Prevent issues with parallel GC
				page.destroy();
			}
		}
		BufferedImage img = imageFromDisplayList(displayList, bounds, new Matrix().scale(zoom));
		displayList.destroy();
		log.trace("Finished rendering document page returning image with dimension {}x{}.", img.getWidth(), img.getHeight());
		return img;

	}

	@Override
	public int getPageCount() throws PdfRendererException {
		return pageCount;
	}

	@Override
	public synchronized PageInfo getPageInfo(int pageNum) throws PdfRendererException {

		log.debug("Assemblying page info for document page {}", pageNum);

		Page page = document.loadPage(pageNum - 1);
		PageInfo pageInfo;
		try {
			pageInfo = getPageInfo(page);
		} finally { // Prevent issues with parallel GC
			page.destroy();
		}

		log.trace("Finished assembling page info.");

		return pageInfo;

	}

	private PageInfo getPageInfo(Page page) {
		Rect bounds = page.getBounds();

		PageInfo pageInfo = new PageInfo();
		pageInfo.setWidth(Math.abs(bounds.x1 - bounds.x0));
		pageInfo.setHeight(Math.abs(bounds.y1 - bounds.y0));

		return pageInfo;
	}

	private BufferedImage imageFromPixmap(Pixmap pixmap) {

		int w = pixmap.getWidth();
		int h = pixmap.getHeight();

		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		int[] pixels = pixmap.getPixels();

		int[] imgData = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(pixels, 0, imgData, 0, pixels.length);

		return image;

	}

	private BufferedImage imageFromDisplayList(DisplayList displayList, Rect bbox, Matrix ctm) {
		Pixmap pixmap = new Pixmap(ColorSpace.DeviceBGR, bbox.transform(ctm), true);
		pixmap.clear(0xff); // set a white background

		DrawDevice dev = new DrawDevice(pixmap);
		try {
			displayList.run(dev, ctm, null);
		} finally { // make sure the device is always closed
			dev.close();
		}
		dev.destroy();

		BufferedImage image = imageFromPixmap(pixmap);
		pixmap.destroy();

		return image;
	}

	@Override
	public long getMemoryFootprint() {
		return memoryFootprint;
	}

	@Override
	public String toString() {
		return String.format("MuPdfRendererImpl [memoryFootprint=%s, pageCount=%s]", FileUtils.byteCountToDisplaySize(memoryFootprint), pageCount);
	}

	@Override
	public void dispose() {
		// do nothing as renderer may still be used by other threads
	}

}
